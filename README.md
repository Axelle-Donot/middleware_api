# Middleware_api

## Getting started

### Install

Clone the project 

```
cd Middleware_api
npm i 

```
### Connect BDD

Rename the dbConfigTemplate.js by dbConfig.js
Modify the connection information in this file

### Start

```
npm start
```


