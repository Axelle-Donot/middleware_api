
const g_productDB = require("../dbConfig").g_productDB;
const { json } = require("express");
const { g_cartDB, g_userDB, e_cartDB } = require("../dbConfig");


//Return if the product is already in the cart of the client
function checkIfProductInCart(productId, email, callBack) {
  g_userDB.query("SELECT id FROM user WHERE user.email = ?",
    [email],
    (err, results) => {
      if (err) {
        console.error("Erreur lors de la récupération des infos user", err);
        callback({ error: "Erreur interne du serveur" });
        return;
      }
      //------------------------Recherche si le produit est dj dans le cart du user---------------------------
      userId = results[0].id
      g_cartDB.query("SELECT c.id AS cart_id, cp.id_cart, cp.id_product, cp.quantity FROM cart AS c JOIN cartproduct AS cp ON c.id = cp.id_cart WHERE c.id_client = ? AND cp.id_product = ?;",
        [userId, productId],
        (err, results) => {
          if (err) {
            console.error("Erreur lors de la récupération des infos user", err);
            callback({ error: "Erreur interne du serveur" });
            return;
          }
          if (results.length > 0) {
            callBack(null, { cartId: results[0].cart_id, isExist: true })
          } else {
            callBack(null, { userId: userId, isExist: false })
          }

        }
      )
    });
}



exports.getAllProducts = (req, res) => {
  g_productDB.query("SELECT * FROM product", (err, results) => {
    if (err) {
      console.error("Erreur lors de la récupération des produits :", err);
      res.status(500).json({ error: "Erreur interne du serveur" });
      return;
    }
    res.json(results);
  });
};


exports.addProduct = (req, res) => {
  const { productId, email } = req.body;
  checkIfProductInCart(productId, email, (err, jsonData) => {
    if (err) {
      // Gérer l'erreur ici
      console.error(err);
      return 500;
    }
    if (jsonData.isExist) {
      cartId = jsonData.cartId
      ret = e_cartDB.query("UPDATE cartproduct AS cp JOIN cart AS c ON cp.id_cart = c.id SET cp.quantity = cp.quantity + 1 WHERE c.id = ? AND cp.id_product = ?;",
        [cartId, productId],
        (err, results) => {
          if (err) {
            console.error("Erreur lors de la récupération des infos user", err);
            //callback({ error: "Erreur interne du serveur" });
            res.json(500);
          }
          res.json(200);

        }
      )
    } else {
      userId = jsonData.userId
      g_cartDB.query("SELECT id FROM cart WHERE id_client = ?;", [userId],
        (err, results) => {
          if (err) {
            console.error("Erreur lors de la récupération des infos user", err);
            //callback({ error: "Erreur interne du serveur" });
            res.json(500);
          }
          cartId = results[0].id
          ret = e_cartDB.query("INSERT INTO `cartproduct` (`id_cart`, `id_product`, `quantity`) VALUES (?, ?, '1');",
            [cartId, productId],
            (err, results) => {
              if (err) {
                console.error("Erreur lors de la récupération des infos user", err);
                //callback({ error: "Erreur interne du serveur" });
                res.json(500);
              }
              res.json(200)
            }
          )
        }
      )
    }
  });
};

exports.removeProduct = (req, res) => {
  const { productId, email } = req.body;
  checkIfProductInCart(productId, email, (err, jsonData) => {
    if (err) {
      // Gérer l'erreur ici
      console.error(err);
      return 500;
    }
    if (jsonData.isExist) {
      cartId = jsonData.cartId
      
    } else {
      userId = jsonData.userId
      
    }
  });
}