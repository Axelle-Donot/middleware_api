const mysql = require("mysql2");

const connection = mysql.createConnection({
  host: "localhost",
  user: "", // root on xampp or wamp
  password: "", // empty for xampp or wamp
  database: "middleware_car",
  port: 3333,
});

const g_userDB = mysql.createPool({
  connectionLimit: 10,
  host: 'localhost',
  user: 'g_user',
  password: 'g_user_password',
  database: 'middleware_car',
});

const e_userDB = mysql.createPool({
  connectionLimit: 10,
  host: 'localhost',
  user: 'e_user',
  password: 'e_user_password',
  database: 'middleware_car',
});

const g_productDB = mysql.createPool({
  connectionLimit: 10,
  host: 'localhost',
  user: 'g_product',
  password: 'g_product_password',
  database: 'middleware_car',
});

const g_cartDB = mysql.createPool({
  connectionLimit: 10,
  host: 'localhost',
  user: 'g_cart',
  password: 'g_cart_password',
  database: 'middleware_car',
});

const e_cartDB = mysql.createPool({
  connectionLimit: 10,
  host: 'localhost',
  user: 'e_cart',
  password: 'e_cart_password',
  database: 'middleware_car',
});

const r_cartDB = mysql.createPool({
  connectionLimit: 10,
  host: 'localhost',
  user: 'r_cart',
  password: 'r_cart_password',
  database: 'middleware_car',
});


module.exports = {
  connection,
  e_cartDB,
  e_userDB,
  g_cartDB,
  g_productDB,
  g_userDB,
  r_cartDB
};



