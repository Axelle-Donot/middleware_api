const express = require("express")
const app = express()
const userRoutes = require("./routes/userRoutes")
const productRoutes = require("./routes/productsRoutes")
const cartRoutes = require("./routes/cartRoutes")

const connection = require("./dbConfig").connection // Utiliser le fichier dbConfig.js
const loginRoutes = require("./routes/loginRoutes")
const cors = require("cors")
const bodyParser = require("body-parser")

const swaggerUi = require("swagger-ui-express")
const swaggerJsdoc = require("swagger-jsdoc")

// Middleware pour parser le JSON
app.use(express.json())

// Vérifier la connexion à la base de données
connection.connect((err) => {
  if (err) {
    console.error("Erreur de connexion à la base de données :", err)
    return
  }
  console.log("Connecté à la base de données MySQL !")
})

//MiddleWare
app.use(cors())
app.use(bodyParser.json())

// -----------------------------------Documentation
const swaggerOptions = {
  swaggerDefinition: {
    info: {
      version: "1.0.0",
      title: "Cars_API",
      description: "Cars API MiddleWare",
      contact: {
        name: "Amazing Developer",
      },
      servers: ["http://localhost:4000"],
    },
  },
  apis: ["./routes/*.js"],
}

const swaggerDocs = swaggerJsdoc(swaggerOptions)
app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs))

// Utiliser les routes définies
app.get("/", (req, res) => {
  res.status(200).send("API MiddleWareCar")
})
app.use(userRoutes)
app.use(productRoutes)
app.use(loginRoutes)
app.use(cartRoutes)

// Démarrer le serveur
const port = 4000
app.listen(port, () => {
  console.log(`Le serveur est en cours d'exécution sur le port ${port}`)
})
