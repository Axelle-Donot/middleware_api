const express = require("express")
const router = express.Router()
const cartController = require("../controllers/cartController")
const verifyToken = require("../middleware") // Importer le middleware de vérification du token depuis le fichier middleware.js

router.post("/cart", verifyToken, cartController.getCart)

module.exports = router
