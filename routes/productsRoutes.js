const express = require("express")
const router = express.Router()
const productController = require("../controllers/productsController")
const verifyToken = require("../middleware") // Importer le middleware de vérification du token depuis le fichier middleware.js

/**
 * @swagger
 * /products:
 *  get:
 *    description: Use to request all products
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get("/product/all", productController.getAllProducts) //PUBLIC API NO TOKEN VERIFICATION
router.post("/product/add", verifyToken, productController.addProduct)
//router.get("/product/remove", verifyToken, productController.removeProduct);
//router.get("/product/remove/all",verifyToken, productController.removeAllProduct);

// Autres routes liées aux produits

module.exports = router
