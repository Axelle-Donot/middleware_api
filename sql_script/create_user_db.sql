CREATE USER 'g_user'@'localhost' IDENTIFIED BY 'g_user_password';
CREATE USER 'e_user'@'localhost' IDENTIFIED BY 'e_user_password';
CREATE USER 'g_product'@'localhost' IDENTIFIED BY 'g_product_password';
CREATE USER 'g_cart'@'localhost' IDENTIFIED BY 'g_cart_password';
CREATE USER 'e_cart'@'localhost' IDENTIFIED BY 'e_cart_password';
CREATE USER 'r_cart'@'localhost' IDENTIFIED BY 'r_cart_password';

GRANT SELECT ON middleware_car.user TO 'g_user'@'localhost';

GRANT INSERT, UPDATE ON middleware_car.user TO 'e_user'@'localhost';
GRANT SELECT ON middleware_car.user TO 'e_user'@'localhost';


GRANT SELECT ON middleware_car.product TO 'g_product'@'localhost';

GRANT SELECT ON middleware_car.cart TO 'g_cart'@'localhost';
GRANT SELECT ON middleware_car.cartproduct TO 'g_cart'@'localhost';

GRANT INSERT, UPDATE ON middleware_car.cart TO 'e_cart'@'localhost';
GRANT INSERT, UPDATE ON middleware_car.cartproduct TO 'e_cart'@'localhost';
GRANT SELECT ON middleware_car.cart TO 'e_cart'@'localhost';
GRANT SELECT ON middleware_car.cartproduct TO 'e_cart'@'localhost';


GRANT DELETE ON middleware_car.cart TO 'r_cart'@'localhost';
GRANT DELETE ON middleware_car.cartproduct TO 'r_cart'@'localhost';
GRANT SELECT ON middleware_car.cart TO 'r_cart'@'localhost';
GRANT SELECT ON middleware_car.cartproduct TO 'r_cart'@'localhost';

