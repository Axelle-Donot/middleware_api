-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mer. 02 août 2023 à 12:39
-- Version du serveur : 10.4.27-MariaDB
-- Version de PHP : 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

/* Version : 1.1.0 */

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `middleware_car`
--

-- --------------------------------------------------------

--
-- Structure de la table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `id_client` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cartproduct`
--

CREATE TABLE `cartproduct` (
  `id_cart` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `price` float NOT NULL,
  `img_name` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `img_name`) VALUES
(13, 'Citroen C3', 5000, 'c3.jpg'),
(14, 'Audi A1', 3000, 'a1.jpg'),
(15, 'Suzuki Splash', 3000, 'splash.jpg'),
(16, 'Suzuki Swift', 5000, 'swift.jpg'),
(17, 'Peugeot 107', 1000, '107.jpg'),
(18, 'Peugeot 207', 2000, '207.jpg'),
(19, 'Peugeot 307', 3000, '307.jpg'),
(20, 'Peugeot 407', 4000, '407.jpg'),
(21, 'Peugeot RCZ', 10000, 'rcz.jpg'),
(22, 'Toyota Supra MK4', 60000, 'mk4.jpg'),
(23, 'Bugatti Bolide', 10000000, 'bolide.jpg'),
(24, 'Subaru WRZ', 50000, 'wrz.jpg'),
(25, 'Mercedes-Benz C-Class', 35000, 'classc.jpg'),
(26, 'BMW 3 Series', 40000, '3series.jpg'),
(27, 'Audi A4', 38000, 'a4.jpg'),
(28, 'Lexus ES', 42000, 'es.jpg'),
(29, 'Volvo S60', 35000, 's60.jpg'),
(30, 'Tesla Model 3', 45000, 'model3.jpg'),
(31, 'Ford Mustang', 32000, 'mustang.jpg'),
(32, 'Chevrolet Camaro', 35000, 'camaro.jpg'),
(33, 'Dodge Charger', 38000, 'charger.jpg'),
(34, 'Mazda MX-5 Miata', 30000, 'mx5.jpg'),
(35, 'Nissan GT-R', 80000, 'gtr.jpg'),
(36, 'Porsche 911', 100000, '911.jpg'),
(37, 'Aston Martin DB11', 200000, 'db11.jpg'),
(38, 'Ferrari F8 Tributo', 300000, 'f8tributo.jpg'),
(39, 'Lamborghini Urus', 250000, 'urus.jpg'),
(40, 'Jeep Wrangler', 40000, 'wrangler.jpg'),
(41, 'Ford Bronco', 35000, 'bronco.jpg'),
(42, 'Toyota 4Runner', 38000, '4runner.jpg'),
(43, 'Land Rover Range Rover', 80000, 'rangerover.jpg'),
(44, 'Lexus RX', 50000, 'rx.jpg'),
(45, 'BMW X5', 60000, 'x5.jpg\n'),
(46, 'Audi Q7', 55000, 'q7.jpg'),
(47, 'Porsche Cayenne', 70000, 'cayenne.jpg'),
(48, 'Tesla Model X', 90000, 'modelx.jpg'),
(49, 'Kia Telluride', 40000, 'telluride.jpg'),
(50, 'Mazda CX-9', 35000, 'cx9.jpg'),
(51, 'Toyota Highlander', 38000, 'highlander.jpg'),
(52, 'Honda Pilot', 35000, 'pilot.jpg'),
(53, 'Chevrolet Traverse', 40000, 'traverse.jpg'),
(54, 'GMC Acadia', 38000, 'acadia.jpg'),
(55, 'Nissan Armada', 45000, 'armada.jpg'),
(56, 'Toyota Sequoia', 50000, 'sequoia.jpg'),
(57, 'Ford Expedition', 55000, 'expedition.jpg'),
(58, 'Chevrolet Tahoe', 55000, 'tahoe.jpg'),
(59, 'GMC Yukon', 60000, 'yukon.jpg'),
(60, 'Honda Odyssey', 35000, 'odyssey.jpg'),
(61, 'Chrysler Pacifica', 38000, 'pacifica.jpg'),
(62, 'Toyota Sienna', 40000, 'sienna.jpg'),
(63, 'Kia Carnival', 35000, 'carnival.jpg'),
(64, 'Mazda Mazda5', 30000, 'mazda5.jpg'),
(65, 'Nissan Quest', 32000, 'quest.jpg'),
(66, 'Honda Ridgeline', 35000, 'ridgeline.jpg'),
(67, 'Toyota Tacoma', 38000, 'tacoma.jpg'),
(68, 'Ford Ranger', 35000, 'ranger.jpg'),
(69, 'Chevrolet Colorado', 32000, 'colorado.jpg'),
(70, 'GMC Canyon', 33000, 'canyon.jpg'),
(71, 'Nissan Frontier', 30000, 'frontier.jpg'),
(72, 'Toyota Tundra', 40000, 'tundra.jpg'),
(73, 'Ram 1500', 38000, '1500.jpg'),
(74, 'Ford F-150', 40000, 'f150.jpg'),
(75, 'Chevrolet Silverado', 38000, 'silverado.jpg'),
(76, 'GMC Sierra', 40000, 'sierra.jpg'),
(77, 'Nissan Titan', 35000, 'titan.jpg'),
(78, 'Honda HR-V', 30000, 'hrv.jpg'),
(79, 'Kia Seltos', 32000, 'seltos.jpg'),
(80, 'Mazda CX-3', 30000, 'cx3.jpg'),
(81, 'Nissan Kicks', 32000, 'kicks.jpg'),
(82, 'Toyota C-HR', 35000, 'chr.jpg'),
(83, 'Ford Escape', 32000, 'escape.jpg'),
(84, 'Chevrolet Equinox', 30000, 'equinox.jpg'),
(85, 'GMC Terrain', 32000, 'terrain.jpg'),
(86, 'Honda CR-V', 35000, 'crv.jpg'),
(87, 'Kia Sportage', 32000, 'sportage.jpg'),
(88, 'Mazda CX-5', 35000, 'cx5.jpg'),
(89, 'Nissan Rogue', 38000, 'rogue.jpg'),
(90, 'Toyota RAV4', 38000, 'rav4.jpg'),
(91, 'Jeep Compass', 35000, 'compass.jpg'),
(92, 'Ford Edge', 32000, 'edge.jpg'),
(93, 'Chevrolet Blazer', 35000, 'blazer.jpg'),
(95, 'Nissan Murano', 35000, 'murano.jpg'),
(96, 'Honda Passport', 38000, 'passport.jpg'),
(97, 'Toyota Venza', 35000, 'venza.jpg'),
(98, 'Audi Q5', 50000, 'q5.jpg'),
(100, 'Mercedes-Benz GLC', 55000, 'glc.jpg'),
(101, 'Volvo XC60', 52000, 'xc60.jpg'),
(102, 'Land Rover Range Rover Velar', 60000, 'rangerovervelar.jpg'),
(103, 'Lexus NX', 55000, 'nx.jpg'),
(104, 'Mitsubishi Outlander', 32000, 'outlander.jpg'),
(105, 'Subaru Forester', 35000, 'forester.jpg'),
(107, 'Jeep Grand Cherokee', 45000, 'grandcherokee.jpg'),
(108, 'Ford Explorer', 48000, 'explorer.jpg'),
(155, 'BMW X3', 58000, 'x3.jpg\n'),
(300, 'Lamborghini Huracan', 250000, 'huracan.jpg'),
(301, 'Porsche 911 Carrera', 100000, '911carrera.jpg'),
(302, 'Chevrolet Corvette', 80000, 'corvette.jpg'),
(303, 'Audi R8', 150000, 'r8.jpg'),
(304, 'BMW M4', 80000, 'm4.jpg'),
(306, 'Ford Mustang Shelby GT500', 70000, 'mustangshelbygt500.jpg'),
(307, 'McLaren 720S', 300000, '720s.jpg'),
(308, 'Ferrari 488 GTB', 250000, '488gtb.jpg'),
(309, 'Mercedes-AMG GT', 150000, 'amggt.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`) VALUES
(1, 'Loutron', 'loutron@loutron.com', 'loutron@loutron.com');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_idclient` (`id_client`);

--
-- Index pour la table `cartproduct`
--
ALTER TABLE `cartproduct`
  ADD KEY `fk_idproduct` (`id_product`),
  ADD KEY `fk_idcart` (`id_cart`);

--
-- Index pour la table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=310;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `fk_id_idclient` FOREIGN KEY (`id_client`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `cartproduct`
--
ALTER TABLE `cartproduct`
  ADD CONSTRAINT `fk_idcart` FOREIGN KEY (`id_cart`) REFERENCES `cart` (`id`),
  ADD CONSTRAINT `fk_idproduct` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
